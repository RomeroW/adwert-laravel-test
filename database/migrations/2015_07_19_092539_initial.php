<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function ($table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('ip');
            $table->string('country');
            $table->string('browser');
        });

        Schema::create('attachments', function ($table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('file');
        });

        Schema::create('adwerts', function ($table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->dateTime('created_at');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('attach_id')->nullable();
        });

        Schema::table('adwerts', function($table) {

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('attach_id')
                ->references('id')->on('attachments')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('migrations');
        Schema::drop('attachments');
        Schema::drop('adwertsphp artisan migrate:rollback');
    }
}
