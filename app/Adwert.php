<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adwert extends Model
{
    public $timestamps = false;
    protected $table = 'adwerts';

    protected $fillable = ['title', 'description'];

    /**
     * Attachment, attached to adwert
     */
    public function attachment()
    {
        return $this->hasOne('App\Attachment', 'id', 'attach_id');
    }

    /**
     * Attachment's owner
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
