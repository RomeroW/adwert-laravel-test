<?php

namespace App\Http\Controllers;

use App\Adwert;
use App\Attachment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class AdwertController extends Controller
{

    /**
     * Get one adwert and render it into one view
     * @param $id
     */
    public function getAdwert($id)
    {
        $adwert = Adwert::findOrFail($id);

        return view('adwert.one', [
            'adwert' => $adwert
        ]);
    }

    /**
     * Get end show adwerts list
     */
    public function getList()
    {
        $adwerts = Adwert::orderBy('id', 'desc')->get();
        return view('adwert.list', ['adwerts' => $adwerts]);
    }

    /**
     * Handle creation of adwert
     * @param Request $request
     */
    public function addNew(Request $request)
    {
        if ($request->isMethod('post')) {

            $this->validate($request, [
                'title' => 'required|max:25',
                'description' => 'required',
                'attachment' => 'image'
            ]);

            $model = new Adwert;

            $model->title = strip_tags($request->input('title'));
            $model->description = strip_tags($request->input('description'));
            $model->created_at = date('Y-m-d H:i:s');

            $model->save();

            // Handle attachment
            if(Input::file())
            {
                $image = Input::file('attachment');
                $filename  = time() . '.' . $image->getClientOriginalExtension();

                $path = public_path('images/' . $filename);

                Image::make($image->getRealPath())->resize(200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path);

                $attachment = new Attachment;
                $attachment->file = $filename;
                $attachment->save();

                $model->attach_id = $attachment->id;
                $model->save();
            }

            // Handle user data
            $user = new User;
            $user->ip = $_SERVER['REMOTE_ADDR'];

            $data = @unserialize(file_get_contents('http://ip-api.com/php/'.$_SERVER['REMOTE_ADDR']));
            if($data && $data['status'] == 'success') {
                $country = $data['country'];
            }
            else {
                $country = 'Unable to get location';
            }
            $user->country = $country;

            $browserData = getBrowser();
            $user->browser = $browserData['name'];
            $user->save();

            $model->user_id = $user->id;
            $model->save();
        }

        return view('adwert.new');
    }
}
