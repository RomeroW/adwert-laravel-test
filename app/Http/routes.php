<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'AdwertController@getList');

Route::get('/{id}', 'AdwertController@getAdwert')
    ->where(['id' => '[0-9]+']);

Route::match(['get', 'post'], '/new', 'AdwertController@addNew');