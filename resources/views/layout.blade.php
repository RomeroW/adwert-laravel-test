<!DOCTYPE html>
<html>
<head>
    <title>Adwertise your stuff</title>
    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/css/main.css">
</head>
<body>
<div class="container">
    @yield('content')
</div>
</body>
</html>
