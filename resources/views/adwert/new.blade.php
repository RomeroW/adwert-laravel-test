@extends('layout')

@section('content')

<h1>Add new adwert</h1>

@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form class="create" method="post" enctype="multipart/form-data">
    <input type="text" name="title" placeholder="Adwert title">
    <textarea name="description" placeholder="Description"></textarea>
    <input type="file" name="attachment">
    {!! csrf_field() !!}

    <input type="submit" value="Add">
</form>

@stop