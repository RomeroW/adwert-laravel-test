@extends('layout')

@section('content')
<h1>ADWERTS LIST</h1>
<div class="adwerts">
    <a class="adwert new" href="/new">
        <h2>+</h2>
    </a>
    @if (isset($adwerts))
    @foreach ($adwerts as $adwert)
    <a class="adwert" href="/{{ $adwert->id }}">
        @if ($attach = $adwert->attachment)
        <img src="/images/{{$attach->file}}"/>
        @endif
        <div class="product_title">{{ $adwert->title }}</div>
        <div class="date">{{ $adwert->created_at}}</div>
        <div class="description">{{ $adwert->description }}</div>
    </a>
    @endforeach
    @endif
</div>
@stop