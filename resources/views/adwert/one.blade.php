@extends('layout')

@section('content')
    <h1>{{ $adwert->title }}</h1>
    <div class="adwerts">
        <div class="back"><a href="/">Back to list</a></div>
        @if ($attach = $adwert->attachment)
        <img src="/images/{{$attach->file}}"/>
        @endif
        <div class="date">{{ $adwert->created_at}}</div>
        <div class="description">{{ $adwert->description }}</div>
        @if ($user = $adwert->user)
        <div class="stat">
            <div>IP: {{hideIP($user->ip)}}</div>
            <div>Location: {{$user->country}}</div>
            <div>Browser: {{$user->browser}}</div>
        </div>
        @endif
    </div>
@stop